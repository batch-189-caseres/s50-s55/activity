// import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'
import {Fragment, useEffect, useState} from 'react';

export default function Courses() {
	// If we will be calling a JS we must enclosed it with {}
	// console.log(coursesData)
	// purpose of key is to verify that the item is unique
	
	const [courses, setCourses] = useState([])


	useEffect(() => {

		fetch('http://localhost:4000/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
		
		return (
				<CourseCard key={course.id} courseProp={course}/>
			)
		}))
		})
	}, [])

	// const courses = coursesData.map(course => {
	// 	return (
	// 			<CourseCard key={course.id} courseProp={course}/>
	// 		)
	// })

	return (
		
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
		
		
		)
}