import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard'
import {Fragment} from 'react';

export default function Home() {

	return(

		<Fragment>
			<Banner/>
			<Highlights/>
			{/*// <CourseCard/>*/}
		</Fragment>

		)
}