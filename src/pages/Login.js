import { useState, useEffect, useContext } from 'react';
import {Form, Button}  from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

// Allows us to consume the USer context object and its properties to use for our user validation
const { user,setUser} = useContext(UserContext)

const [email, setEmail] = useState("");
const [password, setPassword] = useState("");


const [isActive, setIsActive] = useState(false);



useEffect(() => {

	if(email !== "" && password !== ""){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password])

function authenticate(e) {
	e.preventDefault()


	/*
	option may contain header, body etc..
		Syntax: 
			fetch('url', {options} )
			.then(res => res.json())
			.then (data => {})


	*/

	fetch('http://localhost:4000/users/login', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(typeof data.access !== "undefined") {
			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access)

			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to Zuitt!"
			})
		} else {

			Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Wrong email or password"
			})
		}
	})

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}` 
			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}



	// Set th email of the authenticated use in the local storage
	/*
		Syntax:
		 localStorage.setItem("propertyName", value)

	*/
	// localStorage.setItem("email", email)

	// Set the global user state to have properties obtained from local storage
	// setUser({
	// 	email: localStorage.getItem('email')
	// })



	setEmail("")
	setPassword("")

}

return(


	(user.id !== null ) ?
		<Navigate to="/courses" />
		:
	<Form className="mt-3" onSubmit={(e)=> authenticate(e)}>
	<h1>Login</h1>
	<Form.Group className="mb-3" controlId="userEmail">
	<Form.Label>Email address</Form.Label>
	<Form.Control 
				type="email" 
				placeholder="Enter email"
				value={email} 
				onChange={e => {
					setEmail(e.target.value)
				}}
				required />
	<Form.Text className="text-muted">
	We'll never share your email with anyone else.
	</Form.Text>
	</Form.Group>

	<Form.Group className="mb-3" controlId="password">
	<Form.Label>Password</Form.Label>
	<Form.Control 
				type="password" 
				placeholder="Password" 
				value={password}
				onChange={e => {
					setPassword(e.target.value)
				}}
				required />
	</Form.Group>
	
	{
		isActive ?
			<Button variant="primary" type="submit" id="submitBtn">
	   		Submit
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn" disabled>
			Submit
			</Button>
	}
	</Form>



	)
}