import {Row, Col, Button} from 'react-bootstrap';
import '../App.css';
export default function Banner () {

	return(
// The className is now called class name prop or properties
			<Row>
				<Col className="p-5 text-center">
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere.</p>
					<Button variant="primary">Enroll Now!</Button>
				</Col>
			</Row>



		)
}