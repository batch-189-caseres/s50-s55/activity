// import { useState, useEffect } from 'react';
import {Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default function CourseCard({courseProp}) {
	//Deconstruct the course properties into their own variable
	//  instead of prop.courseProp.name
	
	/*
	Use the state hook for this component to be able to store its state.
	States are used to keep track of information related to individual components

	Syntax:
		const [getter, setter] = useState(initialGetterValue)

	*/

	// const [count, setCount] = useState(0);
 //    const [seat, setSeat] = useState(30);
	// function enroll() {
	// 	// if (seat > 0){
	// 		setCount(count + 1);
	// 		setSeat(seat - 1)
	// 	    console.log('Enrollees: ' + count)
			
	// // 	} else {
	// // 		alert('No more seats allowed')
	// // 	}
	// }

	// useEffect(() => {
	// 	if (seat === 0) {
	// 		alert('No more seats available')
	// 	}
	// }, [seat])

	const { name, description, price, _id } = courseProp;
	console.log(courseProp)
	return (

			
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>							
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
						{/* <Link className="btn btn-primary" to="/courseView">Details</Link> */}
					</Card.Body>
				</Card>

		)
}