// copy the Imports
// TWO TYPES OF IMPORT IN REACT
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import {useState} from 'react'
import {useContext} from 'react'
import { Link } from 'react-router-dom';
import { Nav, Navbar, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
//This is how we export module in React
// Use Pascal naming in order to Notice that the function is exported
export default function AppNavBar () {
  // This will be the state to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem("email"));
  /*
    getItem() is a method that returns the value of a specified object item

    Sysntax:
      localStorage.getItem("propetyName");


  */

  const { user } = useContext(UserContext) 

	return (

//href is now called 'to'
//as={Link} - it acts as a or anchor tag in HTML
//we can still use href for global sites
		 <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Batch 189</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link  as={Link} to="/courses">Courses</Nav.Link>

            {

                (user.id !== null) ?
                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                  :
                  <>
                  <Nav.Link  as={Link} to="/login">Login</Nav.Link>
                  <Nav.Link  as={Link} to="/register">Register</Nav.Link>
                  </>

            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
}