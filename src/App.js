import CourseView from './pages/CourseView';
import {useState} from 'react';
import {BrowserRouter as Router} from 'react-router-dom'; //as means renaming into another name like Route
import {Routes, Route} from 'react-router-dom'; 
import { useEffect } from 'react';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home';
import { Container } from 'react-bootstrap';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  // State hook for the user state that's defined here for a global scope.
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // This function is for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {


        if(typeof data._id !== "undefined") {

          setUser({
            id:data._id,
            isAdmin: data.isAdmin
          })

        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
    })



  }, [])

  return (
    // import fragment component to act as a parent element
    // or just use empty tag as a parent element
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
   <AppNavBar/>
    <Container>
     
       <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/courses" element={<Courses/>} />
          <Route path="/courses/:courseId" element={<CourseView/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="*" element={<NotFound/>} />
        </Routes>
    </Container>
    </Router>
  </UserProvider>
  
  );
}

export default App;
