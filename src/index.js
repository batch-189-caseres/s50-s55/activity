import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavBar from './AppNavBar'
// Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode> {/*{Purpose is to highlight potential problems in our application; helps to return errors}*/}
    <App />
  </React.StrictMode>
);


/*const name = "Kristian Caseres";
const student = {
  firstName: 'Daniela',
  lastName: 'Aquino'
}

function userName(user) {
  return user.firstName + ' ' + user.lastName
}
const element = <h1>Hello, {userName(student)} </h1>

root.render(element)*/